#!/usr/bin/env python3

"""
Copyright © 2021 Luke Ganzevoort

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import argparse
import asyncio
import logging
import os
import shlex
import time
import csv

from aioconsole import ainput

from joycontrol import logging_default as log, utils
from joycontrol.command_line_interface import ControllerCLI
from joycontrol.controller import Controller
from joycontrol.controller_state import ControllerState, button_push, button_press, button_release
from joycontrol.memory import FlashMemory
from joycontrol.protocol import controller_protocol_factory
from joycontrol.server import create_hid_server
from joycontrol.transport import NotConnectedError

logger = logging.getLogger(__name__)

"""Emulates Switch controller. Opens joycontrol.command_line_interface to send button commands and more.

While running the cli, call "help" for an explanation of available commands.

Usage:
    run_controller_cli.py <controller> [--device_id | -d  <bluetooth_adapter_id>]
                                       [--spi_flash <spi_flash_memory_file>]
                                       [--reconnect_bt_addr | -r <console_bluetooth_address>]
                                       [--log | -l <communication_log_file>]
                                       [--nfc <nfc_data_file>]
    run_controller_cli.py -h | --help

Arguments:
    controller      Choose which controller to emulate. Either "JOYCON_R", "JOYCON_L" or "PRO_CONTROLLER"

Options:
    -d --device_id <bluetooth_adapter_id>   ID of the bluetooth adapter. Integer matching the digit in the hci* notation
                                            (e.g. hci0, hci1, ...) or Bluetooth mac address of the adapter in string
                                            notation (e.g. "FF:FF:FF:FF:FF:FF").
                                            Note: Selection of adapters may not work if the bluez "input" plugin is
                                            enabled.

    --spi_flash <spi_flash_memory_file>     Memory dump of a real Switch controller. Required for joystick emulation.
                                            Allows displaying of JoyCon colors.
                                            Memory dumps can be created using the dump_spi_flash.py script.

    -r --reconnect_bt_addr <console_bluetooth_address>  Previously connected Switch console Bluetooth address in string
                                                        notation (e.g. "FF:FF:FF:FF:FF:FF") for reconnection.
                                                        Does not require the "Change Grip/Order" menu to be opened,

    -l --log <communication_log_file>       Write hid communication (input reports and output reports) to a file.

    --nfc <nfc_data_file>                   Sets the nfc data of the controller to a given nfc dump upon initial
                                            connection.
"""

controller_timing = {
    'a':None,
    'b':None,
    'x':None,
    'y':None,
    'plus':None,
    'minus':None,
    'home':None,
    'left':None,
    'right':None,
    'up':None,
    'down':None,
    'capture':None,
    'l_stick':None,
    'r_stick':None,
    'l':None,
    'r':None,
    'zl':None,
    'zr':None,
    'l_stick_analog':None,
    'r_stick_analog':None
}




def set_button_status(button, pushed=True, secs=None):

    assert button in controller_timing, f"Invalid button {button}"

    print(f"{'Pressing' if pushed else 'Releasing'} button '{button}'.")

    if secs is None:
        controller_timing[button] = None
    else:
        controller_timing[button] = time.time() + secs

    controller_state.button_state.set_button(button, pushed=pushed)


def set_analog_stick_position(stick, x, y, secs=None):

    assert -1 <= x <= 1, f"x value invalid {x}"
    assert -1 <= y <= 1, f"y value invalid {y}"
    assert stick in ['l_stick_analog', 'r_stick_analog'], f"stick value invalid {stick}"

    stick_state = getattr(controller_state, f"{stick[0]}_stick_state")

    print(f"Moving {stick[0]} analog stick to x={x}, y={y}.")

    cal = stick_state.get_calibration()

    if x >= 0:
        h_position = int(cal.h_center + (cal.h_max_above_center * x))
    else:
        h_position = int(cal.h_center + (cal.h_max_below_center * x))

    if y >= 0:
        v_position = int(cal.v_center + (cal.v_max_above_center * y))
    else:
        v_position = int(cal.v_center + (cal.v_max_below_center * y))

    if secs is None:
        controller_timing[stick] = None
    else:
        controller_timing[stick] = time.time() + secs

    stick_state.set_h(h_position)
    stick_state.set_v(v_position)


async def check_status(delay=None):

    start = time.time()
    await controller_state.send()

    while True:

        cur_time = time.time()
        change = False

        for btn in controller_timing:

            if controller_timing[btn] is None:
                continue

            if cur_time >= controller_timing[btn]:

                if btn in ['l_stick_analog', 'r_stick_analog']:
                    set_analog_stick_position(btn, 0, 0)
                else:
                    set_button_status(btn, pushed=False)
                change = True

        if change:
            await controller_state.send()

        if delay is None or cur_time >= delay + start:
            break


async def run_csv(csv_file):

    t_on = 0.07
    t_delay = 0.15

    with open(csv_file,'r') as data:
        instructions = list(csv.reader(data))

    start_time = time.time()
    last_time = start_time

    for line in instructions:

        t = line[0]
        btn = line[1]
        press_len = line[2]

        if press_len == 'quick_press':
            press_len = t_on
        else:
            press_len = float(press_len)

        if t[0] == '+':
            go_time = float(t[1:]) + last_time
        else:
            go_time = float(t) + start_time

        while time.time() < go_time:
            await check_status(delay=None)

        if btn in ['l_stick_analog', 'r_stick_analog']:
            x = float(line[3])
            y = float(line[4])
            set_analog_stick_position(btn, x, y, press_len)

        elif btn in controller_timing:
            set_button_status(btn, secs=press_len, pushed=True)
        else:
            print(f"No button named {btn}")

        last_time = go_time

    while not all(value is None for value in controller_timing.values()):
        await check_status(delay=None)





def run_cli_cmd_loop():

    t_on = 0.07
    t_delay = 0.15

    while True:

        csv_files = [file for file in sorted(os.listdir('./csv_instructions')) if file[-4:] == '.csv']

        user_input = await ainput(prompt='cmd >> ')

        if not user_input:
            continue

        elif user_input in ['exit', 'quit']:
            break

        elif user_input in ['?', 'help']:
            
            csv_files

        elif user_input in csv_files or user_input.isdigit():

            file_to_run = csv_files[int(user_input)] if user_input.isdigit() else user_input

            user_input = await ainput(prompt=f"How many times would you like to run {file_to_run}? >> ")

            for _ in range(int(user_input)):
                await run_csv(file_to_run)

        else:

            for command in user_input.split('&&'):

                cmd, *args = shlex.split(command)

                if cmd in controller_timing:

                    if cmd in ['l_stick_analog', 'r_stick_analog']:
                        set_analog_stick_position(cmd, x=float(args[0]), y=float(args[1]), secs=float(args[2]))
                        await check_status(delay=float(args[2])+t_delay)
                    else:
                        set_button_status(cmd, secs=t_on)
                        await check_status(delay=t_delay)


def print_help():

    
    print("exit")


async def my_run(args):

    # parse the spi flash
    if args.spi_flash:
        with open(args.spi_flash, 'rb') as spi_flash_file:
            spi_flash = FlashMemory(spi_flash_file.read())
    else:
        # Create memory containing default controller stick calibration
        spi_flash = FlashMemory()

    # Get controller name to emulate from arguments
    controller = Controller.from_arg(args.controller)

    with utils.get_output(path=args.log, default=None) as capture_file:
        # prepare the the emulated controller
        factory = controller_protocol_factory(controller, spi_flash=spi_flash)
        ctl_psm, itr_psm = 17, 19
        transport, protocol = await create_hid_server(factory, reconnect_bt_addr=args.reconnect_bt_addr,
                                                      ctl_psm=ctl_psm,
                                                      itr_psm=itr_psm, capture_file=capture_file,
                                                      device_id=args.device_id)

        global controller_state
        controller_state = protocol.get_controller_state()


        try:
            run_cli_cmd_loop()

        finally:
            logger.info('Stopping communication...')
            await transport.close()




if __name__ == '__main__':

    # check if root
    if not os.geteuid() == 0:
        raise PermissionError('Script must be run as root!')

    # setup logging
    #log.configure(console_level=logging.ERROR)
    log.configure()

    parser = argparse.ArgumentParser()
    parser.add_argument('controller', help='JOYCON_R, JOYCON_L or PRO_CONTROLLER')
    parser.add_argument('-l', '--log')
    parser.add_argument('-d', '--device_id')
    parser.add_argument('--spi_flash')
    parser.add_argument('-r', '--reconnect_bt_addr', type=str, default=None,
                        help='The Switch console Bluetooth address, for reconnecting as an already paired controller')
    parser.add_argument('--nfc', type=str, default=None)
    args = parser.parse_args()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(
        my_run(args)
    )
