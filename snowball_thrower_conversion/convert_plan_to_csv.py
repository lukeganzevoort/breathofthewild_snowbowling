"""
Copyright © 2021 Luke Ganzevoort

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import os


cycle_len = 0.024

with open('snowball_thrower.txt') as f:

    lines = f.read().split('\n')

    """
    'snowball_thrower.txt' excerpt for parsing:

    { A,          5 }, // <Continue> // Camera transition takes place after this
    { NOTHING,   50 },
    { B,          5 },
    { NOTHING,   20 }, // If you can knock over all 10 pins in one roll, that's a strike
    { A,          5 }, // <Continue>
    """

csv_lines = []
t = 0

for line in lines:

    open_idx = line.find('{')
    close_idx = line.find('}')
    
    if open_idx == -1 or close_idx == -1:
        continue

    contents = line[open_idx+1:close_idx]

    cmd = contents.split(',')[0].strip()
    cycles = int(contents.split(',')[1].strip())
    duration = cycles * cycle_len
    duration = 'quick_press' if duration < 0.15 else duration

    if cmd == "UP":
        csv_lines.append(f"+{t},l_stick_analog,{duration},0,1")
    elif cmd == "DOWN":
        csv_lines.append(f"+{t},l_stick_analog,{duration},0,-1")
    elif cmd == "LEFT":
        csv_lines.append(f"+{t},l_stick_analog,{duration},-1,0")
    elif cmd == "RIGHT":
        csv_lines.append(f"+{t},l_stick_analog,{duration},1,0")
    elif cmd == "X":
        csv_lines.append(f"+{t},x,{duration}")
    elif cmd == "Y":
        csv_lines.append(f"+{t},y,{duration}")
    elif cmd == "A":
        csv_lines.append(f"+{t},a,{duration}")
    elif cmd == "B":
        csv_lines.append(f"+{t},b,{duration}")
    elif cmd == "L":
        csv_lines.append(f"+{t},l,{duration}")
    elif cmd == "R":
        csv_lines.append(f"+{t},r,{duration}")
    elif cmd == "THROW":
        csv_lines.append(f"+{t},r,{duration}")
    elif cmd == "NOTHING":
        pass
    elif cmd == "TRIGGERS":
        csv_lines.append(f"+{t},l,{duration}")
        csv_lines.append(f"+{t},r,{duration}")
    else:
        print("Command not recognized.")

    t = max(round(cycles * cycle_len, 3), 0.3)


with open('snowball_thrower.csv', 'w') as f:

    f.write('\n'.join(csv_lines))